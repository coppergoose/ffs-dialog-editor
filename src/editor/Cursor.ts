import Scene from "@/editor/Scene";
import SceneObject from "@/editor/SceneObject";

class Cursor {
  x: number;
  y: number;
  previousX: number;
  previousY: number;
  currentDragObject: SceneObject;

  constructor(scene: Scene) {
    this.x = 0;
    this.y = 0;
    this.previousX = 0;
    this.previousY = 0;
    this.currentDragObject = scene.dummyDragObject;
  }

  update(scene: Scene) {
    // let offsetX = this.previousX - this.currentDragObject.x
    // let offsetY = this.previousY - this.currentDragObject.y

    this.currentDragObject.x = this.x - this.currentDragObject.width / 2;
    this.currentDragObject.y = this.y - 8;
  }
}

export default Cursor;
