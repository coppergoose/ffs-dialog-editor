import Scene from "@/editor/Scene";

interface SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  update(scene: Scene): any;
  draw(ctx: CanvasRenderingContext2D, scene: Scene): any;
}

export default SceneObject;
