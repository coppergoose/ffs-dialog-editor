import Scene from "@/editor/Scene";
import KeyCode from "@/editor/helpers/KeyCode";
import KeyStatus from "@/editor/helpers/KeyStatus";
import MouseKeyCode from "@/editor/helpers/MouseKeyCode";

class Camera {
  x: number;
  y: number;
  zoom: number;
  zoomSpeed = 0.0125;
  horizontalSpeed = 4;
  verticalSpeed = 4;
  acceleration = 4;

  readonly minZoom = 0.1;
  readonly maxZoom = 5;

  constructor() {
    this.x = 0;
    this.y = 0;
    this.zoom = 1;
  }

  init() {
    return;
  }

  calcNewCoords(scene: Scene) {
    let acceleration = 1;
    if (scene.inputHandler.getKeyStatus(KeyCode.SHIFT) === KeyStatus.DOWN)
      acceleration = this.acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.LEFT_ARROW) === KeyStatus.DOWN)
      this.x += this.horizontalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.A) === KeyStatus.DOWN)
      this.x += this.horizontalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.RIGHT_ARROW) === KeyStatus.DOWN)
      this.x -= this.horizontalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.D) === KeyStatus.DOWN)
      this.x -= this.horizontalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.UP_ARROW) === KeyStatus.DOWN)
      this.y += this.verticalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.W) === KeyStatus.DOWN)
      this.y += this.verticalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.DOWN_ARROW) === KeyStatus.DOWN)
      this.y -= this.verticalSpeed * acceleration;

    if (scene.inputHandler.getKeyStatus(KeyCode.S) === KeyStatus.DOWN)
      this.y -= this.verticalSpeed * acceleration;

    if (
      scene.inputHandler.getKeyStatus(KeyCode.LEFT_BRACKET) === KeyStatus.DOWN
    )
      this.zoom -= this.zoomSpeed;

    if (
      scene.inputHandler.getKeyStatus(KeyCode.RIGHT_BRACKET) === KeyStatus.DOWN
    )
      this.zoom += this.zoomSpeed;

    // if(scene.inputHandler.getMouseKeyStatus(MouseKeyCode.RIGHT_BUTTON) === KeyStatus.DOWN)
    // {
    //     let offsetX = scene.cursor.x - scene.cursor.previousX
    //     let offsetY = scene.cursor.y - scene.cursor.previousY
    //
    //     this.x += offsetX / (1 / this.zoom)
    //     this.y += offsetY / (1 / this.zoom)
    // }
  }
}

export default Camera;
