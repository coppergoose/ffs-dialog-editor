import Camera from "@/editor/Camera";
import Scene from "@/editor/Scene";

class Canvas {
  width: number;
  height: number;
  scene: Scene;

  ctx: CanvasRenderingContext2D | null;
  canvas: HTMLCanvasElement;
  camera: Camera;

  constructor(canvas: Vue | Element | Vue[] | Element[]) {
    this.canvas = canvas as HTMLCanvasElement;
    this.ctx = this.canvas.getContext("2d");
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.camera = new Camera();
    this.scene = new Scene(this.camera, this);

    this.init();
  }

  init() {
    window.addEventListener("resize", this.onResize);
    this.camera.init();

    if (this.ctx == null) return;
    this.onResize();
    this.drawLoop();
  }

  onResize = () => {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.width = this.canvas.width;
    this.height = this.canvas.height;
  };

  drawLoop = () => {
    this.camera.calcNewCoords(this.scene);
    this.scene.updateLoop();
    requestAnimationFrame(this.drawLoop);

    if (this.ctx == null) return;

    this.ctx.save();
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.width, this.height);
    this.ctx.restore();
    this.ctx.setTransform(
      this.camera.zoom,
      0,
      0,
      this.camera.zoom,
      this.camera.x,
      this.camera.y
    );

    this.ctx.fillStyle = "#212121";
    this.ctx.fillRect(0, 0, this.width, this.height);

    for (let i = 0; i < this.scene.objects.length; i++) {
      this.scene.objects[i].draw(this.ctx, this.scene);
    }
  };
}

export default Canvas;
