import SceneObject from "@/editor/SceneObject";
import Scene from "@/editor/Scene";
import OutputSocket from "@/editor/sockets/OutputSocket";
import MouseKeyCode from "@/editor/helpers/MouseKeyCode";
import KeyStatus from "@/editor/helpers/KeyStatus";

class InputSocket implements SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  radius: number;

  connections: OutputSocket[];

  constructor(radius = 16) {
    this.radius = radius;
    this.width = this.radius;
    this.height = this.radius;
    this.x = 0;
    this.y = 0;
    this.connections = [];
  }

  update(scene: Scene) {
    this.width = this.radius;
    this.height = this.radius;
  }

  draw(ctx: CanvasRenderingContext2D, scene: Scene) {
    if (
      scene.collision.circlePoint(
        this.x,
        this.y,
        this.radius,
        scene.cursor.x,
        scene.cursor.y
      )
    ) {
      if (
        scene.inputHandler.getMouseKeyStatus(MouseKeyCode.LEFT_BUTTON) ===
        KeyStatus.DOWN
      ) {
        ctx.fillStyle = "orange";
        if (scene.cursor.currentDragObject === scene.dummyDragObject)
          scene.cursor.currentDragObject = this;
      } else {
        ctx.fillStyle = "green";
        scene.cursor.currentDragObject = scene.dummyDragObject;
      }
    } else {
      ctx.fillStyle = "limegreen";
    }

    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
  }
}

export default InputSocket;
