import SceneObject from "@/editor/SceneObject";
import Scene from "@/editor/Scene";
import InputSocket from "@/editor/sockets/InputSocket";

class OutputSocket implements SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  radius: number;

  connections: InputSocket[];

  constructor(radius = 16) {
    this.radius = radius;
    this.width = this.radius;
    this.height = this.radius;
    this.x = 0;
    this.y = 0;

    this.connections = [];
  }

  update(scene: Scene) {
    this.width = this.radius;
    this.height = this.radius;
  }

  draw(ctx: CanvasRenderingContext2D, scene: Scene) {
    if (
      scene.collision.circlePoint(
        this.x,
        this.y,
        this.radius,
        scene.cursor.x,
        scene.cursor.y
      )
    ) {
      ctx.fillStyle = "red";
    } else {
      ctx.fillStyle = "salmon";
    }

    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
  }
}

export default OutputSocket;
