enum KeyStatus {
  NONE,
  UP,
  PRESS,
  DOWN,
}

export default KeyStatus;
