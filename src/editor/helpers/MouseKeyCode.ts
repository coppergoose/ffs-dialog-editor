enum MouseKeyCode {
  LEFT_BUTTON,
  MIDDLE_BUTTON,
  RIGHT_BUTTON,
}

export default MouseKeyCode;
