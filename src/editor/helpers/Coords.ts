import Scene from "@/editor/Scene";

class Coords {
  scene: Scene;

  constructor(scene: Scene) {
    this.scene = scene;
  }

  screenToCanvasX(x: number) {
    return (x - this.scene.camera.x) * (1 / this.scene.camera.zoom);
  }

  screenToCanvasY(y: number) {
    return (y - this.scene.camera.y) * (1 / this.scene.camera.zoom);
  }

  canvasToScreenX(x: number) {
    return x / (1 / this.scene.camera.zoom) + this.scene.camera.x;
  }

  canvasToScreenY(y: number) {
    return y / (1 / this.scene.camera.zoom) + this.scene.camera.y;
  }
}

export default Coords;
