enum KeyCode {
  LEFT_ARROW = 37,
  RIGHT_ARROW = 39,
  UP_ARROW = 38,
  DOWN_ARROW = 40,
  W = 87,
  A = 65,
  S = 83,
  D = 68,
  SHIFT = 16,
  LEFT_BRACKET = 219,
  RIGHT_BRACKET = 221,
}

export default KeyCode;
