import Scene from "@/editor/Scene";

class Collision {
  scene: Scene;

  constructor(scene: Scene) {
    this.scene = scene;
  }

  rectPoint(
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    x3: number,
    y3: number
  ) {
    return x3 >= x1 && x3 <= x2 && y3 >= y1 && y3 <= y2;
  }

  circlePoint(x1: number, y1: number, radius: number, x2: number, y2: number) {
    const distX = x2 - x1;
    const distY = y2 - y1;
    const distance = Math.sqrt(distX ** 2 + distY ** 2);

    return distance <= radius;
  }
}

export default Collision;
