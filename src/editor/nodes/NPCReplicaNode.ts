import ReplicaNode from "@/editor/nodes/ReplicaNode";
import Scene from "@/editor/Scene";
import Text from "@/editor/text/Text";
import MouseKeyCode from "@/editor/helpers/MouseKeyCode";
import KeyStatus from "@/editor/helpers/KeyStatus";
import InputSocket from "@/editor/sockets/InputSocket";
import OutputSocket from "@/editor/sockets/OutputSocket";

class NPCReplicaNode implements ReplicaNode {
  width: number;
  height: number;

  x: number;
  y: number;

  title: string;
  titleText: Text;

  inputSockets: InputSocket[];
  outputSockets: OutputSocket[];

  constructor(title: string) {
    this.width = 300;
    this.height = 350;

    this.x = 0;
    this.y = 0;

    this.title = title;
    this.titleText = new Text(this.title, "#111111");
    this.titleText.align = "center";

    this.inputSockets = [];
    this.inputSockets.push(new InputSocket());
    this.outputSockets = [];
    this.outputSockets.push(new OutputSocket());
  }

  update(scene: Scene) {
    this.titleText.x = this.x + this.width / 2;
    this.titleText.y = this.y + this.titleText.size;
    this.titleText.maxWidth = this.width - 16;
    this.titleText.update(scene);

    for (let i = 0; i < this.inputSockets.length; i++) {
      const socket = this.inputSockets[i];

      // socket.x = this.x - socket.width
      // socket.y = this.y + this.height / 2
      socket.update(scene);
    }

    for (let i = 0; i < this.outputSockets.length; i++) {
      const socket = this.outputSockets[i];

      socket.x = this.x + this.width + socket.width;
      socket.y = this.y + this.height / 2;
      socket.update(scene);
    }
  }

  draw(ctx: CanvasRenderingContext2D, scene: Scene) {
    if (
      scene.collision.rectPoint(
        this.x,
        this.y,
        this.x + this.width,
        this.y + this.height,
        scene.cursor.x,
        scene.cursor.y
      )
    ) {
      if (
        scene.inputHandler.getMouseKeyStatus(MouseKeyCode.LEFT_BUTTON) ===
        KeyStatus.DOWN
      ) {
        ctx.fillStyle = "skyblue";
        if (scene.cursor.currentDragObject === scene.dummyDragObject)
          scene.cursor.currentDragObject = this;
      } else {
        ctx.fillStyle = "cornflowerblue";
        scene.cursor.currentDragObject = scene.dummyDragObject;
      }
    } else {
      ctx.fillStyle = "lightskyblue";
    }

    ctx.fillRect(this.x, this.y, this.width, this.height);

    for (let i = 0; i < this.inputSockets.length; i++) {
      const socket = this.inputSockets[i];
      const x1 = socket.x;
      const y1 = socket.y;

      ctx.beginPath();
      ctx.moveTo(x1, y1);
      for (let i = 0; i < socket.connections.length; i++) {
        const connection = socket.connections[i];
        const x2 = connection.x;
        const y2 = connection.y;

        const midX = (x2 + x1) / 2;
        const midY = (y2 + y1) / 2;

        ctx.bezierCurveTo(midX, y1, midX, y1, midX, midY);
        ctx.moveTo(x2, y2);
        ctx.bezierCurveTo(midX, y2, midX, y2, midX, midY);
      }
      ctx.lineWidth = 6;
      ctx.strokeStyle = "#f1f1f1";
      ctx.stroke();
      ctx.lineWidth = 1;
      ctx.strokeStyle = "#111";
      ctx.stroke();
      socket.draw(ctx, scene);
    }

    for (let i = 0; i < this.outputSockets.length; i++) {
      const socket = this.outputSockets[i];
      const x1 = socket.x;
      const y1 = socket.y;

      ctx.beginPath();
      ctx.moveTo(x1, y1);
      for (let i = 0; i < socket.connections.length; i++) {
        const connection = socket.connections[i];
        const x2 = connection.x;
        const y2 = connection.y;

        const midX = (x2 + x1) / 2;
        const midY = (y2 + y1) / 2;

        ctx.bezierCurveTo(midX, y1, midX, y1, midX, midY);
        ctx.moveTo(x2, y2);
        ctx.bezierCurveTo(midX, y2, midX, y2, midX, midY);
      }
      ctx.lineWidth = 6;
      ctx.strokeStyle = "#f1f1f1";
      ctx.stroke();
      ctx.lineWidth = 1;
      ctx.strokeStyle = "#111";
      ctx.stroke();
      socket.draw(ctx, scene);
    }

    this.titleText.draw(ctx, scene);
  }
}

export default NPCReplicaNode;
