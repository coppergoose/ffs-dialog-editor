import Scene from "@/editor/Scene";
import SceneObject from "@/editor/SceneObject";
import Text from "@/editor/text/Text";
import InputSocket from "@/editor/sockets/InputSocket";
import OutputSocket from "@/editor/sockets/OutputSocket";

interface ReplicaNode extends SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  title: string;
  titleText: Text;

  inputSockets: InputSocket[];
  outputSockets: OutputSocket[];

  update(scene: Scene): any;
  draw(ctx: CanvasRenderingContext2D, scene: Scene): any;
}

export default ReplicaNode;
