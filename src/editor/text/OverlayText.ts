import Scene from "@/editor/Scene";
import Constants from "@/editor/Constants";
import SceneObject from "@/editor/SceneObject";

class OverlayText implements SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  maxWidth: number;

  content: string;
  size: number;
  color: string;
  align: "center" | "end" | "left" | "right" | "start";

  constructor(content: string, color = "#fff", size = 16) {
    this.width = 0;
    this.height = 0;
    this.content = content;
    this.color = color;
    this.size = size;
    this.x = 0;
    this.y = 0;
    this.maxWidth = 65536;
    this.align = "left";
  }

  update(scene: Scene) {
    return;
  }

  draw(ctx: CanvasRenderingContext2D, scene: Scene) {
    ctx.fillStyle = this.color;
    ctx.font = `${this.size * (1 / scene.camera.zoom)}px ${Constants.font}`;
    ctx.textAlign = this.align;
    ctx.fillText(
      this.content,
      scene.coordsHelper.screenToCanvasX(this.x),
      scene.coordsHelper.screenToCanvasY(this.y),
      this.maxWidth
    );
  }
}

export default OverlayText;
