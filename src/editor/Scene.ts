import Cursor from "@/editor/Cursor";
import Camera from "@/editor/Camera";
import InputHandler from "@/editor/InputHandler";
import SceneObject from "@/editor/SceneObject";
import Coords from "@/editor/helpers/Coords";
import MCReplicaNode from "@/editor/nodes/MCReplicaNode";
import NPCReplicaNode from "@/editor/nodes/NPCReplicaNode";
import OverlayText from "@/editor/text/OverlayText";
import Text from "@/editor/text/Text";
import Canvas from "@/editor/Canvas";
import Collision from "@/editor/helpers/Collision";
import DummyDragObject from "@/editor/DummyDragObject";

class Scene {
  readonly coordsHelper: Coords;

  canvas: Canvas;
  camera: Camera;
  cursor: Cursor;
  objects: SceneObject[];
  inputHandler: InputHandler;
  collision: Collision;

  fpsText: OverlayText;
  cursorText: OverlayText;
  zoomText: OverlayText;

  dummyDragObject: SceneObject;

  constructor(camera: Camera, canvas: Canvas) {
    this.objects = [];
    this.dummyDragObject = new DummyDragObject();
    this.canvas = canvas;
    this.cursor = new Cursor(this);
    this.coordsHelper = new Coords(this);
    this.inputHandler = new InputHandler(this.canvas);
    this.collision = new Collision(this);
    this.camera = camera;
    this.fpsText = new OverlayText("FPS", "#fff", 20);
    this.cursorText = new OverlayText("Cursor", "#fff", 20);
    this.zoomText = new OverlayText("Zoom", "#fff", 20);

    this.init();
  }

  init() {
    const node1 = new MCReplicaNode();
    const node2 = new NPCReplicaNode("NPC");
    node1.x = 64;
    node1.y = 72;
    node2.x = 440;
    node2.y = 72;
    this.fpsText.x = 16;
    this.fpsText.y = 28;
    this.cursorText.x = 16;
    this.cursorText.y = 48;
    this.zoomText.x = 16;
    this.zoomText.y = 68;

    this.objects.push(node1, node2);
    this.objects.push(this.fpsText, this.cursorText, this.zoomText);

    node1.outputSockets[0].connections.push(node2.inputSockets[0]);
    // node1.inputSockets[0].connections.push(node2.outputSockets[0])
    node2.inputSockets[0].connections.push(node1.outputSockets[0]);
  }

  updateLoop() {
    this.cursor.update(this);
    this.cursorText.content = `Cursor: ${this.cursor.x.toFixed(
      0
    )}, ${this.cursor.y.toFixed(0)}`;
    this.zoomText.content = `Zoom: ${this.camera.zoom.toFixed(2)} / ${
      this.camera.maxZoom
    }`;

    for (let i = 0; i < this.objects.length; i++) {
      this.objects[i].update(this);
    }
  }
}

export default Scene;
