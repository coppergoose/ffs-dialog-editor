import Scene from "@/editor/Scene";
import SceneObject from "@/editor/SceneObject";

class DummyDragObject implements SceneObject {
  width: number;
  height: number;

  x: number;
  y: number;

  constructor() {
    this.x = 0;
    this.y = 0;
    this.width = 0;
    this.height = 0;
  }

  update(scene: Scene) {
    return;
  }

  draw(ctx: CanvasRenderingContext2D, scene: Scene) {
    return;
  }
}

export default DummyDragObject;
