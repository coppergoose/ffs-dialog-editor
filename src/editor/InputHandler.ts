import KeyCode from "@/editor/helpers/KeyCode";
import KeyStatus from "@/editor/helpers/KeyStatus";
import MouseKeyCode from "@/editor/helpers/MouseKeyCode";
import Canvas from "@/editor/Canvas";

class InputHandler {
  canvas: Canvas;
  keys: KeyStatus[];
  mouseKeys: KeyStatus[];

  constructor(canvas: Canvas) {
    this.canvas = canvas;
    this.keys = [];
    this.mouseKeys = [];
    this.init();
  }

  init() {
    let keyIds = Object.keys(KeyCode).filter((k) => !isNaN(Number(k)));
    for (let i = 0; i < keyIds.length; i++) {
      const key = <number>(<unknown>keyIds[i]);
      this.setKeyStatus(<KeyCode>(<unknown>key), KeyStatus.NONE);
    }

    keyIds = Object.keys(MouseKeyCode).filter((k) => !isNaN(Number(k)));
    for (let i = 0; i < keyIds.length; i++) {
      const key = <number>(<unknown>keyIds[i]);
      this.setMouseKeyStatus(<MouseKeyCode>(<unknown>key), KeyStatus.NONE);
    }

    window.addEventListener("keydown", this.onKeydown);
    window.addEventListener("keypress", this.onKeypress);
    window.addEventListener("keyup", this.onKeyup);

    window.addEventListener("mousedown", this.onMousedown);
    window.addEventListener("mousemove", this.onMousemove);
    window.addEventListener("mouseup", this.onMouseup);

    window.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      return false;
    });
  }

  setKeyStatus = (key: KeyCode, status: KeyStatus) => {
    this.keys[key] = status;
  };

  setMouseKeyStatus = (key: MouseKeyCode, status: KeyStatus) => {
    this.mouseKeys[key] = status;
  };

  getKeyStatus = (key: KeyCode): KeyStatus => {
    const oldStatus = this.keys[key];
    if (oldStatus === KeyStatus.UP)
      this.setKeyStatus(<KeyCode>(<unknown>key), KeyStatus.NONE);
    return oldStatus;
  };

  getMouseKeyStatus = (key: MouseKeyCode): KeyStatus => {
    const oldStatus = this.mouseKeys[key];
    if (oldStatus === KeyStatus.UP)
      this.setMouseKeyStatus(<MouseKeyCode>(<unknown>key), KeyStatus.NONE);
    return oldStatus;
  };

  onKeydown = (e: KeyboardEvent) => {
    this.setKeyStatus(<KeyCode>(<unknown>e.keyCode), KeyStatus.DOWN);
    // console.log(<KeyCode><unknown>e.keyCode, KeyCode[<KeyCode><unknown>e.keyCode], KeyStatus[this.getKeyStatus(<KeyCode><unknown>e.keyCode)])
  };

  onKeypress = (e: KeyboardEvent) => {
    this.setKeyStatus(<KeyCode>(<unknown>e.keyCode), KeyStatus.PRESS);
  };

  onKeyup = (e: KeyboardEvent) => {
    this.setKeyStatus(<KeyCode>(<unknown>e.keyCode), KeyStatus.UP);
    // console.log(<KeyCode><unknown>e.keyCode, KeyCode[<KeyCode><unknown>e.keyCode], KeyStatus[this.getKeyStatus(<KeyCode><unknown>e.keyCode)])
  };

  onMousedown = (e: MouseEvent) => {
    this.setMouseKeyStatus(<MouseKeyCode>(<unknown>e.button), KeyStatus.DOWN);
    this.canvas.scene.cursor.previousX = this.canvas.scene.cursor.x;
    this.canvas.scene.cursor.previousY = this.canvas.scene.cursor.y;
    // console.log(<MouseKeyCode><unknown>e.button, MouseKeyCode[<MouseKeyCode><unknown>e.button], KeyStatus[this.getMouseKeyStatus(<MouseKeyCode><unknown>e.button)])
  };

  onMousemove = (e: MouseEvent) => {
    e.preventDefault();

    const rect = this.canvas.canvas.getBoundingClientRect();
    this.canvas.scene.cursor.x = this.canvas.scene.coordsHelper.screenToCanvasX(
      e.clientX - rect.left
    );
    this.canvas.scene.cursor.y = this.canvas.scene.coordsHelper.screenToCanvasY(
      e.clientY - rect.top
    );

    if (this.getMouseKeyStatus(MouseKeyCode.RIGHT_BUTTON) === KeyStatus.DOWN) {
      const offsetX =
        this.canvas.scene.cursor.x - this.canvas.scene.cursor.previousX;
      const offsetY =
        this.canvas.scene.cursor.y - this.canvas.scene.cursor.previousY;

      this.canvas.camera.x += offsetX / (1 / this.canvas.camera.zoom);
      this.canvas.camera.y += offsetY / (1 / this.canvas.camera.zoom);
    }
  };

  onMouseup = (e: MouseEvent) => {
    this.setMouseKeyStatus(<MouseKeyCode>(<unknown>e.button), KeyStatus.UP);
    this.canvas.scene.cursor.previousX = this.canvas.scene.cursor.x;
    this.canvas.scene.cursor.previousY = this.canvas.scene.cursor.y;
    // console.log(<MouseKeyCode><unknown>e.button, MouseKeyCode[<MouseKeyCode><unknown>e.button], KeyStatus[this.getMouseKeyStatus(<MouseKeyCode><unknown>e.button)])
  };
}

export default InputHandler;
